package aula;

public class Time {

    private int segundos;

    /**
     * Constrói um time referente à hora 00:00:00.
     */
    public Time() {
        setTime(0, 0, 0);
    }

    /**
     * Constrói um Time com valores especificados pelos parâmetros. Usa o
     * mecanismo definido no setTime() para definição do horário.
     *
     * @param horas a quantidade de horas
     * @param minutos a quantidade de minutos
     * @param segundos a quantidade de segundos
     */
    public Time(int horas, int minutos, int segundos) {
        setTime(horas, minutos, segundos);
    }

    /**
     * Constrói um Time com o horário recebido como String. Usa o mecanismo
     * definido no setTime() para definição do horário.
     *
     * @param time o horário completo em formato ("hh:mm:ss").
     */
    public Time(String time) {
        String[] valores = time.split(":");
        setTime(
                Integer.parseInt(valores[0]),
                Integer.parseInt(valores[1]),
                Integer.parseInt(valores[2])
        );
    }

    /**
     * Seta os valores de horas, minutos e segundos de acordo com os parâmetros.
     * O excedente em segundos é acrescentado em minutos e o excedente em
     * minutos é acrescentado em horas. Caso seja passado um valor para horas
     * que seja maior ou igual a 24, as horas são setadas como o excedente. Ex.:
     * 25:61:304 vira 02:06:04
     *
     * @param horas a quantidade de horas
     * @param minutos a quantidade de minutos
     * @param segundos a quantidade de segundo
     */
    public void setTime(int horas, int minutos, int segundos) {
        this.segundos = (horas * 3600) + (minutos * 60) + segundos;

    }

    public int getHoras() {
        return segundos / 3600;
    }

    public int getMinutos() {
        return (segundos % 3600) / 60;
    }

    public int getSegundos() {
        return (segundos % 3600) % 60;
    }

    public void setHoras(int horas) {
        segundos += horas * 3600;
    }

    public void setMinutos(int minutos) {
        segundos += minutos + 60;
    }

    public void setSegundos(int segundos) {
        this.segundos += segundos;
    }

    public String toString() {
        return String.format("%02d:%02d:%02d", getHoras()%24, getMinutos(), getSegundos());
    }
    
    public void tick(){
        segundos++;
    }
}
