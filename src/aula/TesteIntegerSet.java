package aula;


public class TesteIntegerSet {

    public static void main(String[] args) {
        IntegerSet i1 = new IntegerSet();
        i1.adicionarElemento(2);
        i1.adicionarElemento(3);
        i1.adicionarElemento(40);
        i1.adicionarElemento(50);
        
        IntegerSet i2 = new IntegerSet();
        i2.adicionarElemento(2);
        i2.adicionarElemento(4);
        i2.adicionarElemento(40);
        i2.adicionarElemento(90);
        
        IntegerSet i3 = new IntegerSet();
        i3.adicionarElemento(2);
        i3.adicionarElemento(3);
        i3.adicionarElemento(40);
        i3.adicionarElemento(50);
        
        // contem()
        System.out.println( i1.contem(2) == true );
        System.out.println( i1.contem(50) == true );
        System.out.println( i1.contem(51) == false );
        System.out.println( i1.contem(102) == false );
        System.out.println( i1.contem(-1) == false );

        // equals()
        System.out.println( i1.equals(i2) == false );
        System.out.println( i1.equals(i3) == true );
        System.out.println( i1.equals(null) == false );

        // uniao()
        IntegerSet esperado = new IntegerSet();
        esperado.adicionarElemento(2);
        esperado.adicionarElemento(3);
        esperado.adicionarElemento(4);
        esperado.adicionarElemento(40);
        esperado.adicionarElemento(50);
        esperado.adicionarElemento(90);
        
        IntegerSet resultadoObtido = i1.uniao(i2);
        System.out.println( esperado.equals(resultadoObtido) );
        resultadoObtido = i1.uniao(null);
        System.out.println( i1.equals(resultadoObtido) );
        
        IntegerSet i4 = new IntegerSet();
        IntegerSet i5 = new IntegerSet();
        esperado = new IntegerSet();
        System.out.println( i4.uniao(i5).equals(esperado) );
        
        
        // intersecao()
        esperado = new IntegerSet();
        esperado.adicionarElemento(2);
        esperado.adicionarElemento(40);
        System.out.println( i1.intersecao(i2).equals(esperado) );
        esperado = new IntegerSet();
        System.out.println( i1.intersecao(null).equals(esperado) );

        // toString()
        System.out.println(i1.toString().equals("{2, 3, 40, 50}"));
    }

}
